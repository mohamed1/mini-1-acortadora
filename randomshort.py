from webapp import WebApp
import random
import string
import shelve
import urllib.parse

class MainPage(WebApp):

    def parse(self, received):
        request = received.decode()
        metodo = request.split(" ")[0]
        if metodo == "POST":
            body = request.split("url=")[1]
            body = urllib.parse.unquote(body)
            recurso = '/'
        else:
            if len(request) >= 2:
                recurso = request.split(" ")[1]
            else:
                recurso = None
            body = None
        return metodo, recurso, body

    def process(self, parsed_request):
        metodo, recurso, body = parsed_request
        with shelve.open('urls.db') as db:
            urls = dict(db)
        if recurso == '/' and metodo == 'GET': #get
            return self.main_page(urls)
        elif recurso != '/' and metodo == 'GET': #get con redirección
            http, content = self.redirect_to_url(recurso, urls)
            return http, content
        else: #post
            if body:
                http, content= self.shorten_url(body, urls)
                with shelve.open('urls.db') as db:
                    urls = dict(db)
                http, contentmas = self.main_page(urls)
                content += contentmas
                return http, content
            else:
                return self.page_not_found()

    def page_not_found(self):
        http = "404 Not Found"
        content =  "<html><body><h1>Page not found</h1></body></html>"
        return http, content

    def get_short_url(self, url, urls):
        if urls and url in urls:
            return urls[url]
        else:
            short_url = self.generate_short_url()
            with shelve.open('urls.db') as db:
                db[short_url] = url
            return short_url

    def generate_short_url(self):
        chars = string.ascii_letters + string.digits
        short_url = '/' + ''.join(random.choice(chars) for _ in range(6))
        return short_url

    def main_page(self, urls):
        content = '''
                    <html>
                        <head><title>URL Shortener</title></head>
                            <body>
                                <form method="post">
                                    <label>Enter URL to shorten:</label>
                                    <input type="text" name="url">
                                    <input type="submit" value="Shorten">
                                </form>
                            <body>
                    <html> '''

        if urls != {}:
            content += '''<h3>Shortened URLs</h3><ul>'''
            for url, short_url in urls.items():
                content += f'<li><a href="{short_url}">{url}</a></li> -->' \
                           f'<a href="{url}">{short_url}</a>'
                '''</ul></body></html>'''
        return "200 OK", content

    def redirect_to_url(self, recurso, urls):
        short_url = recurso
        if short_url in urls.keys():
            for clave in urls.keys():
                if clave == short_url:
                    valor = urls[short_url]
                    http = "HTTP/1.1 301 Moved Permanently\r\n"
                    content = f"Location: {valor}\r\n\r\n"
                    return http, content
        else:
            return self.page_not_found()

    def shorten_url(self, url, urls):
        if not url.startswith('http://') and not url.startswith('https://'):
            url = 'http://' + url
        if not url in urls.values():
            short_url = self.generate_short_url()
            db = shelve.open('urls.db')
            db[short_url] = url
            db.close()
            http = '200 OK'
            content = f"<html><body><h1>The shortened url is {short_url} and it has been added " \
                          f"to the list</h1></body></html>"
            return (http, content)
        else:
            http, content = self.main_page(urls)
            content = f"<html><body><h1>The url has been shortened previously</h1></body></html>"
        return http, content

if __name__ == '__main__':
    web_app = MainPage('localhost', 1234)
