import socket

class WebApp:
    def parse(self, request):
        received = request.decode()
        return received.split(" ")[1]

    def process(self, analyzed):
        http = "200 OK"
        html = "<html><body><h1>Hello!</h1></body></html>"
        return http, html

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.host, self.port))
        self.socket.listen(5)

        while True:
            (client_socket, client_address) = self.socket.accept()
            request = client_socket.recv(1024)
            parsed_request = self.parse(request)
            http, html = self.process(parsed_request)
            if http == "HTTP/1.1 301 Moved Permanently\r\n":
                response = str(http) + str(html)
                client_socket.send(response.encode('utf-8'))
            else:
                response = f"HTTP/1.1 {http}\r\nContent-Type: text/html\r\n\r\n{html}"
                client_socket.sendall(response.encode('utf-8'))
            client_socket.close()

